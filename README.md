0## Installer Openocd

Si ce n'est pas le cas, Les outils suivants doivent être installés : make, libtool, pkg-config, autoconf, automake, texinfo.

`
sudo apt install make libtool pkg-config autoconf automake texinfo
`
Charger les sources d’openocd là où on veut avoir le binaire et compiler (/opt pour l’IRIT).

```
cd /là/où/on/veut/installer/openocd
git clone https://github.com/riscv/riscv-openocd
cd riscv-openocd
mkdir build
./bootstrap
./configure --enable-ftdi --prefix=<absolute path>/build --exec-prefix=<absolute path>/build
make
make install
export PATH=$PATH:<path to riscv-openocd>/build/bin
```

Ajouter une ligne à votre fichier de configuration de shell pour modifier la variable PATH au login.

## Configuration linux pour utiliser le Câble HS2

Connecter la carte vcu118. 3 câbles relient la carte au PC.

![alt text](./docs/pictures/carte_vcu118.jpeg)

Allumer la carte.

Le câble de programmation JTAG-HS2 est un câble qui permet la programmation des Xilinx FPGAs (bitstream loading) à partir d'un port USB du PC (éviter de passer par un hub).
Il est utilisé pour les applications sur le coeur qui est instantié sur le FPGA à travers un connecteur PMOD JE.
Il faut ajouter une règle udev pour utiliser ce câble.
OpenOCD fournit un fichier contenant cette règle.

```
$ sudo cp <path to riscv-openocd>/build/share/openocd/contrib/60-openocd.rules /etc/udev/rules.d
$ sudo udevadm control --reload
```

Pour vérifier que le câble est reconnu, exécuter lsusb. 

```
$ lsusb
```

Une ligne ressemblant à celle-ci doit s'afficher :

```
Bus 005 Device 003: ID 0403:6014 Future Technology Devices International, Ltd FT232HSingle HS USB-UART/FIFO IC
```
Débrancher le câble HS2.

## Installer gdb-multiarch

Sur notre ubuntu, le gdb fourni dans la chaine gnu pour riscv provoque un segmentation fault. On utilisera donc gdb-multiarch à la place.

```
Dans un autre terminal :
Verifier quel ttyUSB est connecté au port uart JB de la carte
déconnecter le câble, ls /dev/ttyUSB*, reconnecter le cable, ls /dev/ttyUSB*. En comparant les deux résultats, on trouve le nouveau dev qui doit être utilisé (ttyUSB0 par la suite).
openocd -f Install_Minotaur/MINOTAuR/fpga/openocd_digilent_hs2.cfg
Si ça ne marche pas, essayer de débrancher et rebrancher (côté PC) le câble relié au port JE.
Laisser ce terminal ouvert.

Dans un nouveau terminal qu’on laisse ouvert pour avoir les retours de l’UART
screen /dev/ttyUSB0 115200
```

## Créer un répertoire pour installer les outils liés au projet

```
mkdir Install_Minotaur
cd Install_Minotaur
```

## Construire le compilateur gcc pour riscv (dans /opt/riscv-gnu-toolchain pour l'IRIT)

```
git clone https://github.com/riscv/riscv-gnu-toolchain 
cd riscv-gnu-toolchain 
git checkout ed53ae7a71dfc6df1940c2255aea5bf542a9c422
```

Ne pas s’inquiéter des commentaires qui s’affichent en retour

```
git submodule update --init --recursive
mkdir path/to/install/riscv/compiler
export RISCV=/ opt/riscv-gnu-toolchain
./configure --prefix="$RISCV" --disable-linux --with-cmodel=medany --with-arch=rv32ima
make newlib
```

Soyez patient... Une fois terminé, modifier la variable PATH et le fichier de configuration du shell.

```
export PATH=$PATH:$RISCV/bin
```

## Installer le support de la carte dans Vivado Enterprise 2024.1

La carte vcu118 n'est disponible que dans vivado enterprise.

Pour l'installer, il suffit d'aller dans Tools > Vivado Store, de cliquer sur l'onglet Boards et de chercher vcu118.

## Installer le code du FPGA et construire les coremark

```
export PATH_VIVADO=/opt/Vivado_Enterprise
git clone https://gitlab.irit.fr/minotaur/printemps.git
cd printemps
git checkout printemps-vcu118
git submodule update --init --recursive
git apply 0001-coremark-modification.patch
cd sw/app
```

Un script lié à coremark est en python2. Dans le fichier ../utils/bin2mem.py, remplacer python par python2.

```
make coremark.mem
cd ../..
make cva6_fpga
```

## Charger le projet dans Vivado.

Pour le moment, connecter uniquement le câble USB vers le port USB JTAG de la carte.
Brancher et allumer la carte vcu118.

Dans vivado :

Open project /fpga/cva6_fpga.xpr

Sous PROGRAM AND DEBUG/Open Hardware Manager

1. Open Target(AutoConnect)
2. Program Device(xcvu9p_0)

Sur la carte, la led bleue s’allume quand le programme est chargé.
Fermer vivado.

Débrancher le câble du port usb JTAG de la carte, et connecter le câble USB vers le port usb UART de la carte, ainsi que le câble avec le module HS2 vers le port pmod. 

## Configurer les communications pour exécuter les coremark sur le coeur

#### Lancer openocd

```
openocd -f Install_Minotaur/printemps/fpga/openocd_digilent_hs2.cfg
```

Si ça ne marche pas, essayer de débrancher et rebrancher (côté PC) le câble relié au port pmod.
Laisser ce terminal ouvert.

#### Ouvrir un hyper-terminal

Dans un terminal :
Verifier quel ttyUSB est connecté au port uart de la carte
déconnecter le câble, `ls /dev/ttyUSB*`, 
reconnecter le cable, `ls /dev/ttyUSB*`. 
En comparant les deux résultats, on trouve les deux nouveaux dev qui correspondent au port uart de la carte (nommés ttyUSB0 et ttyUSB1 par la suite). D'après nos expériences, il faut utiliser le plus grand des deux.

Dans un nouveau terminal qu’on laisse ouvert pour avoir les retours de l’UART

```
sudo screen /dev/ttyUSB1 460800
```

C'est notre hyper-terminal.

#### Lancer gdb

Dans un troisième terminal :

```
gdb-multiarch Install_Minotaur/printemps/sw/app/coremark.riscv
```

Connecter gdb à openocd

```
(gdb) target extended-remote :3333
Info : accepting 'gdb' connection on tcp/3333
```

Charger le coremark dans la mémoire

```
(gdb) load
Loading section .vectors, size 0x80 lma 0x80000000
Loading section .init, size 0x60 lma 0x80000080
Loading section .text, size 0x19010 lma 0x800000e0
Loading section .rodata, size 0x1520 lma 0x800190f0
Loading section .eh_frame, size 0x50 lma 0x8001a610
Loading section .init_array, size 0x4 lma 0x8001a660
Loading section .data, size 0x9d4 lma 0x8001a668
Loading section .sdata, size 0x40 lma 0x8001b040
Start address 0x80000080, load size 110712
Transfer rate: 63 KB/sec, 7908 bytes/write.
```

Exécuter le programme sur le coeur riscv.

```
(gdb) b _exit
(gdb) continue
```

Le résultat s’affiche dans l’hyperterminal connecté à ttyUSB1 (entre autres le score coremark).
