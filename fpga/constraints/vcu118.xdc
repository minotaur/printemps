
set_property -dict {PACKAGE_PIN G31 IOSTANDARD LVDS} [get_ports clk_sys_p]
set_property -dict {PACKAGE_PIN F31 IOSTANDARD LVDS} [get_ports clk_sys_n]

set_property -dict {PACKAGE_PIN BE22 IOSTANDARD LVCMOS18} [get_ports reset]

## To use FTDI FT2232 JTAG
set_property -dict {PACKAGE_PIN AV16 IOSTANDARD LVCMOS18} [get_ports trst_n]
set_property -dict {PACKAGE_PIN AV15 IOSTANDARD LVCMOS18} [get_ports tck]
set_property -dict {PACKAGE_PIN AY15 IOSTANDARD LVCMOS18} [get_ports tdi]
set_property -dict {PACKAGE_PIN AW15 IOSTANDARD LVCMOS18} [get_ports tdo]
set_property -dict {PACKAGE_PIN AY14 IOSTANDARD LVCMOS18} [get_ports tms]

## UART
set_property -dict {PACKAGE_PIN BB21 IOSTANDARD LVCMOS18} [get_ports tx]
set_property -dict {PACKAGE_PIN AW25 IOSTANDARD LVCMOS18} [get_ports rx]

## JTAG
# minimize routing delay

set_max_delay -to [get_ports tdo] 20.000
set_max_delay -from [get_ports tms] 20.000
set_max_delay -from [get_ports tdi] 20.000
set_max_delay -from [get_ports trst_n] 20.000

# reset signal
set_false_path -from [get_ports trst_n]
