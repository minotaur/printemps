# set_property PACKAGE_PIN F23 [get_ports clk_sys_p]
# set_property IOSTANDARD LVDS [get_ports clk_sys_p]

# set_property PACKAGE_PIN E23 [get_ports clk_sys_n]
# set_property IOSTANDARD LVDS [get_ports clk_sys_n]

# set_property PACKAGE_PIN AH18 [get_ports clk_sys_p]
# set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports clk_sys_p]

# set_property PACKAGE_PIN AH17 [get_ports clk_sys_n]
# set_property IOSTANDARD DIFF_SSTL12_DCI [get_ports clk_sys_n]

set_property -dict {PACKAGE_PIN F23 IOSTANDARD LVDS} [get_ports clk_sys_p]
set_property -dict {PACKAGE_PIN E23 IOSTANDARD LVDS} [get_ports clk_sys_n]

set_property -dict {PACKAGE_PIN M11 IOSTANDARD LVCMOS33} [get_ports reset]

## To use FTDI FT2232 JTAG
set_property -dict {PACKAGE_PIN L10 IOSTANDARD LVCMOS33} [get_ports trst_n]
set_property -dict {PACKAGE_PIN L8 IOSTANDARD LVCMOS33} [get_ports tck]
set_property -dict {PACKAGE_PIN K9 IOSTANDARD LVCMOS33} [get_ports tdi]
set_property -dict {PACKAGE_PIN K8 IOSTANDARD LVCMOS33} [get_ports tdo]
set_property -dict {PACKAGE_PIN J9 IOSTANDARD LVCMOS33} [get_ports tms]

## UART
set_property -dict {PACKAGE_PIN H8 IOSTANDARD LVCMOS33} [get_ports tx]
set_property -dict {PACKAGE_PIN G7 IOSTANDARD LVCMOS33} [get_ports rx]

## JTAG
# minimize routing delay

set_max_delay -to [get_ports tdo] 20.000
set_max_delay -from [get_ports tms] 20.000
set_max_delay -from [get_ports tdi] 20.000
set_max_delay -from [get_ports trst_n] 20.000

# reset signal
set_false_path -from [get_ports trst_n]
