# Copyright (c) 2020 Thales.
# 
# Copyright and related rights are licensed under the Solderpad
# License, Version 2.0 (the "License"); you may not use this file except in
# compliance with the License.  You may obtain a copy of the License at
# http://solderpad.org/licenses/SHL-2.0/ Unless required by applicable law
# or agreed to in writing, software, hardware and materials distributed under
# this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
# Author:         Sebastien Jacq - sjthales on github.com

#
# Additional contributions by:
#
#
# script Name:    run_cva6_fpga
# Project Name:   CVA6 softcore
# Language:       tcl
#
# Description:    Script to generate bitstream of CVA6 architecture
#                 in Zybo 7-20 board
#
# =========================================================================== #
# Revisions  :
# Date        Version  Author       Description
# 2020-11-06  0.1      S.Jacq       Created
# =========================================================================== #


set project cva6_fpga
set src_dir $::env(ARIANE_SRC)/src
set include_dir $::env(ARIANE_SRC)/include
set fpga_src_dir $::env(FPGA_SRC)
set ext_ips $::env(EXT_IPS)
set top_module cva6_ultrascale

set_param board.repoPaths [list $env(HOME)/.Xilinx/Vivado/2024.1/xhub/board_store/xilinx_board_store $env(PATH_VIVADO)/Vivado/2024.1/data/xhub/boards]

create_project $project . -force -part $::env(XILINX_PART)
set_property board_part $::env(XILINX_BOARD) [current_project]

# Adding all files recursively from src, include and fpga
add_files -scan_for_includes $src_dir $include_dir $fpga_src_dir
read_ip $ext_ips

# Removing duplicate files
remove_files [get_files -regexp [list ".*/fpu/.*/popcount.sv" ".*/fpu/.*/registers.svh" ".*/pmp/.*/riscv.sv"]]

# Defining registers.svh as a global header
set global_headers [list ".*/registers.svh" ".*/zybo-z7-20.svh"]
set_property -dict { file_type {Verilog Header} is_global_include 1} -objects [get_files -regexp $global_headers]

set_property include_dirs { "src/axi_sd_bridge/include" "../src/common_cells/include" } [current_fileset]

# Setting top module
set_property top $top_module [get_filesets sources_1]

read_verilog -sv {../src/common_cells/include/common_cells/registers.svh}
set registers "../src/common_cells/include/common_cells/registers.svh"

# Disable unused files
reorder_files -auto -disable_unused -fileset [get_filesets sources_1]

# Remove disabled files
# remove_files [get_files -filter {!IS_ENABLED}]

set_msg_config -id {[Synth 8-5858]} -new_severity "info"
set_msg_config -id {[Synth 8-4480]} -limit 1000

add_files -fileset constrs_1 -norecurse constraints/cva6_fpga.xdc
add_files -fileset constrs_1 -norecurse constraints/vcu118.xdc

set_property STEPS.SYNTH_DESIGN.ARGS.RETIMING true [get_runs synth_1]
update_compile_order -fileset [get_filesets sources_1]

set_property generate_synth_checkpoint 0 [get_files $ext_ips]

set_property STEPS.SYNTH_DESIGN.ARGS.RETIMING true [get_runs synth_1]

launch_runs synth_1
wait_on_run synth_1
open_run synth_1

exec mkdir -p reports_cva6_fpga_synth/
exec rm -rf reports_cva6_fpga_synth/*

check_timing -verbose                                                   -file reports_cva6_fpga_synth/$project.check_timing.rpt
report_timing -max_paths 100 -nworst 100 -delay_type max -sort_by slack -file reports_cva6_fpga_synth/$project.timing_WORST_100.rpt
report_timing -nworst 1 -delay_type max -sort_by group                  -file reports_cva6_fpga_synth/$project.timing.rpt
report_utilization -hierarchical                                        -file reports_cva6_fpga_synth/$project.utilization.rpt
report_cdc                                                              -file reports_cva6_fpga_synth/$project.cdc.rpt
report_clock_interaction                                                -file reports_cva6_fpga_synth/$project.clock_interaction.rpt

# set for RuntimeOptimized implementation
set_property "steps.place_design.args.directive" "RuntimeOptimized" [get_runs impl_1]
set_property "steps.route_design.args.directive" "RuntimeOptimized" [get_runs impl_1]

launch_runs impl_1
wait_on_run impl_1
launch_runs impl_1 -to_step write_bitstream
wait_on_run impl_1
open_run impl_1

# reports
exec mkdir -p reports_cva6_fpga_impl/
exec rm -rf reports_cva6_fpga_impl/*
check_timing                                                              -file reports_cva6_fpga_impl/${project}.check_timing.rpt
report_timing -max_paths 100 -nworst 100 -delay_type max -sort_by slack   -file reports_cva6_fpga_impl/${project}.timing_WORST_100.rpt
report_timing -nworst 1 -delay_type max -sort_by group                    -file reports_cva6_fpga_impl/${project}.timing.rpt
report_utilization -hierarchical                                          -file reports_cva6_fpga_impl/${project}.utilization.rpt
