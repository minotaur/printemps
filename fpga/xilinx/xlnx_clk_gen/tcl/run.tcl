set partNumber $::env(XILINX_PART)
set boardName  $::env(XILINX_BOARD)

set ipName xlnx_clk_gen

set_param board.repoPaths [list $env(HOME)/.Xilinx/Vivado/2024.1/xhub/board_store/xilinx_board_store $env(PATH_VIVADO)/Vivado/2024.1/data/xhub/boards]

create_project $ipName . -force -part $partNumber
set_property board_part $boardName [current_project]

create_ip -name clk_wiz -vendor xilinx.com -library ip -module_name $ipName

set_property -dict [list CONFIG.PRIM_IN_FREQ {300.000} \
                        CONFIG.NUM_OUT_CLKS {4} \
                        CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} \
                        CONFIG.CLKOUT1_USED {true} \
                        CONFIG.CLKOUT2_USED {true} \
                        CONFIG.CLKOUT3_USED {true} \
                        CONFIG.CLKOUT4_USED {true} \
                        CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {100} \
                        CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {125} \
                        CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {125} \
                        CONFIG.CLKOUT3_REQUESTED_PHASE {90.000} \
                        CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {50} \
                        CONFIG.CLKIN1_JITTER_PS {50.0} \
                       ] [get_ips $ipName]

generate_target {instantiation_template} [get_files ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
generate_target all [get_files  ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
launch_run -jobs 8 ${ipName}_synth_1
wait_on_run ${ipName}_synth_1
