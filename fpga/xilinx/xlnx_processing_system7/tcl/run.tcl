set partNumber $::env(XILINX_PART)
set boardName  $::env(XILINX_BOARD)

set ipName xlnx_processing_system7

set_param board.repoPaths [list $env(HOME)/.Xilinx/Vivado/2024.1/xhub/board_store/xilinx_board_store $env(PATH_VIVADO)/Vivado/2024.1/data/xhub/boards]

create_project $ipName . -force -part $partNumber
set_property board_part $boardName [current_project]

create_ip -name zynq_ultra_ps_e -vendor xilinx.com -library ip -module_name $ipName

set_property -dict [list \
  CONFIG.PSU__USE__M_AXI_GP0 {0} \
  CONFIG.PSU__USE__M_AXI_GP1 {0} \
  CONFIG.PSU__USE__S_AXI_GP2 {1} \
  CONFIG.PSU__QSPI__PERIPHERAL__ENABLE {0} \
  CONFIG.PSU__USE__FABRIC__RST {0} \
  CONFIG.PSU__FPGA_PL0_ENABLE {0} \
  CONFIG.PSU__USE__IRQ0 {0} \
] [get_ips $ipName]

generate_target {instantiation_template} [get_files ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
generate_target all [get_files  ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
create_ip_run [get_files -of_objects [get_fileset sources_1] ./$ipName.srcs/sources_1/ip/$ipName/$ipName.xci]
launch_run -jobs 8 ${ipName}_synth_1
wait_on_run ${ipName}_synth_1
