module lru_4way (
  input logic [1:0] hit_i,
  input logic miss_i,
  input logic [4:0] ages_i,
  output logic [4:0] ages_o,
  output logic [1:0] evicted_o,
  output logic updated_o
);

  function automatic logic [1:0] ordered_2 (logic [1:0] fstway, logic [1:0] sndway);
    return (fstway != 2'b00) ? 2'b00 : ((sndway == 2'b01) ? 2'b10 : 2'b01);
  endfunction

  function automatic logic [1:0] ordered_3 (logic [1:0] fstway, logic [1:0] sndway);
    return (sndway != 2'b11) ? 2'b11 : ((fstway == 2'b10) ? 2'b01 : 2'b10);
  endfunction

  logic [3:0][1:0] ways, ordered_ways;
  logic [2:0][1:0] new_ways;
  logic [1:0][1:0] new_ways_ordered;
  logic [1:0] predicted_3rd_way;

  always_comb begin : decode
    ways[0] = ages_i[1:0];
    ways[1] = (ways[0] == 2'b00 && ages_i[3:2] == 2'b00) ? 2'b01 : ages_i[3:2];

    ordered_ways[0] = (ways[0] < ways[1]) ? ways[0] : ways[1];
    ordered_ways[1] = (ways[0] < ways[1]) ? ways[1] : ways[0];

    ordered_ways[2] = ordered_2(ordered_ways[0], ordered_ways[1]);
    ordered_ways[3] = ordered_3(ordered_ways[0], ordered_ways[1]);

    ways[2] = (ages_i[4]) ? ordered_ways[3] : ordered_ways[2];
    ways[3] = (ages_i[4]) ? ordered_ways[2] : ordered_ways[3];
  end

  // assign ways[0] = ages_i[1:0];
  // assign ways[1] = (ages_i[1:0] == 0'b00 && ages_i[3:2] == 0'b00) ? 0'b01 : ages_i[3:2];

  // assign ordered_ways[0] = (ways[0] < ways[1]) ? ways[0] : ways[1];
  // assign ordered_ways[1] = (ways[0] < ways[1]) ? ways[1] : ways[0];

  // assign ordered_ways[2] = ordered_2(ordered_ways[0], ordered_ways[1]);
  // assign ordered_ways[3] = ordered_3(ordered_ways[0], ordered_ways[1]);

  // assign ways[2] = (ages_i[4]) ? ordered_ways[3] : ordered_ways[2];
  // assign ways[3] = (ages_i[4]) ? ordered_ways[2] : ordered_ways[3];

  assign evicted_o = ways[3];
  assign updated_o = miss_i || (ways[0] != hit_i) || (ways[1] != ages_i[3:2]);

  always_comb begin : encode
    ages_o = ages_i;
    new_ways = ways[2:0];

    if (miss_i || ways[0] != hit_i) begin
      if (miss_i || ways[1] != hit_i) begin
        new_ways[0] = (miss_i) ? ways[3] : hit_i;
        new_ways[1] = ways[0];
        new_ways[2] = ways[1];
      end else begin
        new_ways[0] = ways[1];
        new_ways[1] = ways[0];
      end
    end

    new_ways_ordered[0] = (new_ways[0] < new_ways[1]) ? new_ways[0] : new_ways[1];
    new_ways_ordered[1] = (new_ways[0] < new_ways[1]) ? new_ways[1] : new_ways[0];

    predicted_3rd_way = ordered_2(new_ways_ordered[0], new_ways_ordered[1]);
    if (new_ways[2] == predicted_3rd_way) begin
      ages_o = {1'b0, new_ways[1], new_ways[0]};
    end else begin
      ages_o = {1'b1, new_ways[1], new_ways[0]};
    end
  end
endmodule
